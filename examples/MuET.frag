#version 400 compatibility

#info MuET (c) Claude Heiland-Allen 2019, license AGPL3+

#include "Trudy-sRGB.frag"
#include "Trudy.frag"

uint hash(uint a) { return hash_burtle_9(a); }

// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

#group MuET

uniform int Iterations; slider[0,100,100000]
uniform float EscapeRadius; slider[0.0,16,100.0]
uniform int Power; slider[2,2,16]
#if __VERSION__ >= 400
uniform dvec2 Seed; slider[(-2,-2),(0,0),(2,2)]
#else
uniform vec2 Seed; slider[(-2,-2),(0,0),(2,2)]
#endif

uniform float time;

#group Colour

uniform float LightDirection; slider[-360,45,360]

#group Formula1

uniform bool Active1; checkbox[true]
uniform bool AbsX1; checkbox[false]
uniform bool AbsY1; checkbox[false]
uniform bool NegX1; checkbox[false]
uniform bool NegY1; checkbox[false]
uniform bool Swap1; checkbox[false]

#group Formula2

uniform bool Active2; checkbox[false]
uniform bool AbsX2; checkbox[false]
uniform bool AbsY2; checkbox[false]
uniform bool NegX2; checkbox[false]
uniform bool NegY2; checkbox[false]
uniform bool Swap2; checkbox[false]

#group Formula3

uniform bool Active3; checkbox[false]
uniform bool AbsX3; checkbox[false]
uniform bool AbsY3; checkbox[false]
uniform bool NegX3; checkbox[false]
uniform bool NegY3; checkbox[false]
uniform bool Swap3; checkbox[false]

#group Formula4

uniform bool Active4; checkbox[false]
uniform bool AbsX4; checkbox[false]
uniform bool AbsY4; checkbox[false]
uniform bool NegX4; checkbox[false]
uniform bool NegY4; checkbox[false]
uniform bool Swap4; checkbox[false]

float EscapeRadius2 = pow(2.0, float(EscapeRadius));
int NActive = int(Active1) + int(Active2) + int(Active3) + int(Active4);
int IterationsN = NActive == 0 ? 0 : Iterations / NActive;
int IterationsM = NActive * IterationsN;

vec2 cMul(vec2 a, vec2 b)
{
  return vec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

vec2 cPow(vec2 a, int n)
{
  // assert(n >= 2);
  vec2 p = a;
  for (int m = 2; m <= n; ++m)
  {
    p = cMul(p, a);
  }
  return p;
}

float dwell(vec2 c)
{
  int i = 0;
  vec2 z = vec2(Seed);
  for (; i < IterationsM;)
  {
    if (Active1)
    {
      if (AbsX1) z.x = abs(z.x);
      if (AbsY1) z.y = abs(z.y);
      if (NegX1) z.x = -z.x;
      if (NegY1) z.y = -z.y;
      if (Swap1) z.xy = z.yx;
      z = cPow(z, Power) + c;
      ++i;
      if (! (dot(z, z) < EscapeRadius2)) break;
    }
    if (Active2)
    {
      if (AbsX2) z.x = abs(z.x);
      if (AbsY2) z.y = abs(z.y);
      if (NegX2) z.x = -z.x;
      if (NegY2) z.y = -z.y;
      if (Swap2) z.xy = z.yx;
      z = cPow(z, Power) + c;
      ++i;
      if (! (dot(z, z) < EscapeRadius2)) break;
    }
    if (Active3)
    {
      if (AbsX3) z.x = abs(z.x);
      if (AbsY3) z.y = abs(z.y);
      if (NegX3) z.x = -z.x;
      if (NegY3) z.y = -z.y;
      if (Swap3) z.xy = z.yx;
      z = cPow(z, Power) + c;
      ++i;
      if (! (dot(z, z) < EscapeRadius2)) break;
    }
    if (Active4)
    {
      if (AbsX4) z.x = abs(z.x);
      if (AbsY4) z.y = abs(z.y);
      if (NegX4) z.x = -z.x;
      if (NegY4) z.y = -z.y;
      if (Swap4) z.xy = z.yx;
      z = cPow(z, Power) + c;
      ++i;
      if (! (dot(z, z) < EscapeRadius2)) break;
    }
  }
  if (! (dot(z, z) < EscapeRadius2))
  {
    return float(i) + 1.0 - log(log(length(z))) / log(float(Power));
  }
  else
  {
    return 0.0;
  }
}

vec3 hue(float d)
{
  float h = floor(fract((d - time) / 3.0) * 3.0);
  if (h == 0.0) return vec3(0.0, 1.0, 1.0);
  if (h == 1.0) return vec3(1.0, 0.0, 1.0);
  return vec3(1.0,1.0,0.0);
}

vec3 shade(float d00, float d01, float d10, float d11)
{
  vec2 e = vec2(d00 - d11, d01 - d10);
  float de = 1.0 / (log(2.0) * length(e));
  float slope = dot(normalize(e), normalize(vec2(cos(radians(LightDirection)), sin(radians(LightDirection)))));
  if (0.0 == d00 * d01 * d10 * d11 || isinf(de) || isnan(de)) return vec3(0.0);
  vec3 h = hsv2rgb(vec3(degrees(atan(e.y, e.x)) / 360.0, clamp(slope, 0.0, 1.0), 1.0));
  return h * tanh(clamp(4.0 * de, 0.0, 4.0));
}

vec3 color(vec2 p, vec2 dx, vec2 dy)
{
  dx *= 0.25;
  dy *= 0.25;
  float d00 = dwell(p - dx - dy);
  float d01 = dwell(p - dx + dy);
  float d10 = dwell(p + dx - dy);
  float d11 = dwell(p + dx + dy);
  return shade(d00, d01, d10, d11);
}

#if __VERSION__ >= 400

dvec2 cMul(dvec2 a, dvec2 b)
{
  return dvec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

dvec2 cPow(dvec2 a, int n)
{
  // assert(n >= 2);
  dvec2 p = a;
  for (int m = 2; m <= n; ++m)
  {
    p = cMul(p, a);
  }
  return p;
}

float dwell(dvec2 c)
{
  int i = 0;
  dvec2 z = dvec2(Seed);
  for (; i < IterationsM;)
  {
    if (Active1)
    {
      if (AbsX1) z.x = abs(z.x);
      if (AbsY1) z.y = abs(z.y);
      if (NegX1) z.x = -z.x;
      if (NegY1) z.y = -z.y;
      if (Swap1) z.xy = z.yx;
      z = cPow(z, Power) + c;
      ++i;
      if (! (dot(z, z) < EscapeRadius2)) break;
    }
    if (Active2)
    {
      if (AbsX2) z.x = abs(z.x);
      if (AbsY2) z.y = abs(z.y);
      if (NegX2) z.x = -z.x;
      if (NegY2) z.y = -z.y;
      if (Swap2) z.xy = z.yx;
      z = cPow(z, Power) + c;
      ++i;
      if (! (dot(z, z) < EscapeRadius2)) break;
    }
    if (Active3)
    {
      if (AbsX3) z.x = abs(z.x);
      if (AbsY3) z.y = abs(z.y);
      if (NegX3) z.x = -z.x;
      if (NegY3) z.y = -z.y;
      if (Swap3) z.xy = z.yx;
      z = cPow(z, Power) + c;
      ++i;
      if (! (dot(z, z) < EscapeRadius2)) break;
    }
    if (Active4)
    {
      if (AbsX4) z.x = abs(z.x);
      if (AbsY4) z.y = abs(z.y);
      if (NegX4) z.x = -z.x;
      if (NegY4) z.y = -z.y;
      if (Swap4) z.xy = z.yx;
      z = cPow(z, Power) + c;
      ++i;
      if (! (dot(z, z) < EscapeRadius2)) break;
    }
  }
  if (! (dot(z, z) < EscapeRadius2))
  {
    return float(i) + 1.0 - log(log(float(length(z)))) / log(float(Power));
  }
  else
  {
    return 0.0;
  }
}

vec3 color(dvec2 p, vec2 dx, vec2 dy)
{
  dx *= 0.25;
  dy *= 0.25;
  float d00 = dwell(p - dx - dy);
  float d01 = dwell(p - dx + dy);
  float d10 = dwell(p + dx - dy);
  float d11 = dwell(p + dx + dy);
  return shade(d00, d01, d10, d11);
}

#endif

#preset Mystery
Zoom = 123456789 Logarithmic
ZoomFactor = 0
EnableTransform = true
RotateAngle = 0
StretchAngle = 0
StretchAmount = 0
Center = -0.05794121488070425808200419,0.8132682994364264091896378
Jitter = 1
Iterations = 16384
EscapeRadius = 16
Power = 2
Seed = 0,0
LightDirection = 45
Active1 = true
AbsX1 = false
AbsY1 = false
NegX1 = false
NegY1 = false
Swap1 = false
Active2 = false
AbsX2 = false
AbsY2 = false
NegX2 = false
NegY2 = false
Swap2 = false
Active3 = false
AbsX3 = false
AbsY3 = false
NegX3 = false
NegY3 = false
Swap3 = false
Active4 = false
AbsX4 = false
AbsY4 = false
NegX4 = false
NegY4 = false
Swap4 = false
Exposure = 2.5
#endpreset
