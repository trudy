#version 330 compatibility

#info (c) Claude Heiland-Allen 2020, license AGPL3+

#include "Trudy-sRGB.frag"
#include "Trudy.frag"

#include "Complex.frag"

const float pi = 3.141592653;

uint hash(uint a) { return hash_burtle_9(a); }

// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

#group Julia

uniform int Iterations; slider[1,100,10000]

uniform int APeriod; slider[1,10,100]
uniform int BPeriod; slider[1,10,100]

// function: z to (z^2 + a) / (z^2 + b)
uniform vec2 A; slider[(-50,-50),(0,0),(50,50)]
uniform vec2 B; slider[(-50,-50),(0,0),(50,50)]

#group Colour

uniform float AHueCenter; slider[0,0.00,1]
uniform float AHueSpread; slider[0,0.25,1]
uniform float AValCenter; slider[0,1.00,1]
uniform float AValSpread; slider[0,1.00,1]

uniform float BHueCenter; slider[0,0.50,1]
uniform float BHueSpread; slider[0,0.25,1]
uniform float BValCenter; slider[0,1.00,1]
uniform float BValSpread; slider[0,1.00,1]

vec2 cProj(vec2 z)
{
  if (isnan(z.x) || isinf(z.x) || isnan(z.y) || isinf(z.y))
    return vec2(1.0 / 0.0, 0.0);
  return z;
}

vec2 cInv(vec2 z)
{
  float z2 = dot(z, z);
  if (isinf(z2)) return vec2(0.0);
  return cConj(z) / z2;
}

vec2 F(vec2 z)
{
  vec2 one = vec2(1.0, 0.0);
  vec2 z2 = cProj(cSqr(z));
  vec2 w1 = cDiv(z2 + A, z2 + B);
  vec2 w2 = cDiv(one + cMul(A, cInv(z2)), one + cMul(B, cInv(z2)));
  if (dot(z2, z2) <= 1.0)
    return cProj(w1);
  if (dot(z2, z2) >= 1.0)
    return cProj(w2);
  return vec2(0.0, 0.0);
}

float D(vec2 a, vec2 b)
{
  vec2 x1 = (a - b);
  vec2 x2 = (cInv(a) - cInv(b));
  float d1 = dot(x1, x1);
  float d2 = dot(x2, x2);
  if (isnan(d1)) d1 = 1.0 / 0.0;
  if (isnan(d2)) d2 = 1.0 / 0.0;
  return min(d1, d2);
}

vec3 color(vec2 p, vec2 dx, vec2 dy)
{
  {
    vec2 zero = vec2(-1.0, 0.0);
    vec2 one = vec2(0.0, 0.0);
    vec2 infinity = vec2(1.0, 0.0);
    vec2 ma = cMul(infinity, zero - one);
    vec2 mb = cMul(zero, one - infinity);
    vec2 mc = zero - one;
    vec2 md = one - infinity;
    p = cDiv(cMul(ma, p) + mb, cMul(mc, p) + md);
  }
  vec2 z    = p;
  vec2 one  = vec2(1.0, 0.0);
  vec2 zero = vec2(0.0, 0.0);
  vec2 inf  = vec2(1.0 / 0.0, 0.0);
  for (int i = 0; i < Iterations; ++i)
  {
    z = F(z);
    zero = F(zero);
    inf = F(inf);
  }
  int a = -1;
  int b = -1;
  float amin = 1.0 / 0.0;
  float bmin = 1.0 / 0.0;
  {
    vec2 w = z;
    for (int i = 0; i < APeriod; ++i)
    {
      w = F(w);
      float d = D(w, zero);
      if (d < amin)
      {
        a = i;
        amin = d;
      }
    }
  }
  {
    vec2 w = z;
    for (int i = 0; i < BPeriod; ++i)
    {
      w = F(w);
      float d = D(w, inf);
      if (d < bmin)
      {
        b = i;
        bmin = d;
      }
    }
  }
  float h0 = mix(AHueCenter, (float(a) + 0.5) / float(APeriod) - 0.5, AHueSpread);
  float h1 = mix(BHueCenter, (float(b) + 0.5) / float(BPeriod) - 0.5, BHueSpread);
  float v0 = mix(AValCenter, (float(a) + 0.5) / float(APeriod), AValSpread);
  float v1 = mix(BValCenter, (float(b) + 0.5) / float(BPeriod), BValSpread);
  float h = amin < bmin ? h0 : h1;
  float u = amin < bmin ? v0 : v1;
  h -= floor(h);
  vec3 c = hsv2rgb(vec3(h, 1.0, 1.0));
  return u * c / dot(c, vec3(0.2126, 0.7152, 0.0722));
}

#preset Default
Zoom = 0.5
Center = 0,0
Iterations = 100
A = 0.060466479382643608,-0.351665081411541403
B = 0.126851826507568627,0.698655823902703865
APeriod = 5
BPeriod = 3
Exposure = -0.3227904
AHueCenter = 0.86206897
AHueSpread = 0.05
AValCenter = 0
AValSpread = 0.12
BHueCenter = 0.10727969
BHueSpread = 0.05
BValCenter = 1
BValSpread = 0.60800001
#endpreset
