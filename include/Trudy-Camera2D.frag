#donotrun

#vertex

#ifndef GL_compatibility_profile
layout(location = 0) in vec4 vertex_position;
uniform mat4 projectionMatrix;
#define gl_Vertex vertex_position
#define gl_ProjectionMatrix projectionMatrix
#endif

out vec2 coord;
out vec2 viewCoord;

#group Camera

uniform float Zoom; slider[1e-4,1,1e16] Logarithmic NotLockable
uniform float ZoomFactor; slider[-100,0,100]
uniform vec2 pixelSize;

uniform bool EnableTransform; checkbox[true]
uniform float RotateAngle; slider[-360,0,360]
uniform float StretchAngle; slider[-360,0,360]
uniform float StretchAmount; slider[-100,0,100]

void main(void)
{
  mat2 transform = mat2(1.0, 0.0, 0.0, 1.0);
  if (EnableTransform)
  {
    float b = radians(RotateAngle);
    float bc = cos(b);
    float bs = sin(b);
    float a = radians(StretchAngle);
    float ac = cos(a);
    float as = sin(a);
    float s = sqrt(pow(2.0, StretchAmount));
    mat2 m1 = mat2(ac, as, -as, ac);
    mat2 m2 = mat2(s, 0.0, 0.0, 1.0 / s);
    mat2 m3 = mat2(ac, -as, as, ac);
    mat2 m4 = mat2(bc, bs, -bs, bc);
    transform = m1 * m2 * m3 * m4;
  }
  float ar = float(pixelSize.y / pixelSize.x);
  coord = transform * ((gl_ProjectionMatrix * gl_Vertex).xy * vec2(ar, 1.0)) / (Zoom * pow(2.0, ZoomFactor));
  viewCoord = gl_Vertex.xy;
  gl_Position =  gl_Vertex;
}

#endvertex

in vec2 coord;
in vec2 viewCoord;

out vec4 fragColor;

#if __VERSION__ >= 400
uniform dvec2 Center; slider[(-100,-100),(0,0),(100,100)] NotLockable
#else
uniform vec2 Center; slider[(-100,-100),(0,0),(100,100)] NotLockable
#endif
uniform float Jitter; slider[0,1,10]
uniform int Samples; slider[1,1,256]
uniform vec2 pixelSize;
uniform int subframe;
uniform sampler2D backbuffer;
uniform float time;

uniform float Shutter; slider[1.0e-6,1.0e-2,1.0e+2]
float Time;

int Sample = 0;

// implement these
vec3 color(vec2 p, vec2 dx, vec2 dy);
#if __VERSION__ >= 400
vec3 color(dvec2 p, vec2 dx, vec2 dy);
#endif

void main()
{
  vec4 next = vec4(0.0);
  vec4 prev = texture(backbuffer, vec2(viewCoord + vec2(1.0)) * 0.5);
  vec2 dx = dFdx(coord);
  vec2 dy = dFdy(coord);
 for (int s = 0; s < Samples; ++s)
 {
  Sample = s;
  int SubFrame = s + subframe * Samples;
  vec3 j = Jitter * vec3
    ( uniform01(hash(vec4(coord, time, float(3 * SubFrame + 0))))
    , uniform01(hash(vec4(coord, time, float(3 * SubFrame + 1))))
    , uniform01(hash(vec4(coord, time, float(3 * SubFrame + 2))))
    );
  vec2 p = coord + j.x * dx + j.y * dy;
  Time = time + Shutter * j.z;
  const float eps = 1.1920928955078125e-07 * 4.0;
  bool double_precision = min(length(dx), length(dy)) < eps && __VERSION__ >= 400;
  vec4 one;
  if (double_precision) one = vec4(color(p + Center, dx, dy), 1.0);
  else                  one = vec4(color(p + vec2(Center), dx, dy), 1.0);
  if (! (one == one)) // NaN check
  {
    one = vec4(0.0);
  }
  next += one;
 }
  fragColor = prev + next;
}
